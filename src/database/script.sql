-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        lallemand
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 10:53
-- Created:       2021-12-20 10:44
PRAGMA foreign_keys = OFF;

-- Schema: mydb
-- ATTACH "mydb.sdb" AS "mydb";
-- comment : enlever les "mysql".
BEGIN;
CREATE TABLE "devs"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "firstname" VARCHAR(45),
  "lastname" VARCHAR(45),
  "skill" VARCHAR(45)
);
CREATE TABLE "clients"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45),
  "address" VARCHAR(45)
);
CREATE TABLE "projects"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45),
  "content" VARCHAR(255),
  "clients_id" INTEGER NOT NULL,
  CONSTRAINT "fk_projects_clients1"
    FOREIGN KEY("clients_id")
    REFERENCES "clients"("id")
);
CREATE INDEX "projects.fk_projects_clients1_idx" ON "projects" ("clients_id");
CREATE TABLE "projects_has_devs"(
  "projects_id" INTEGER NOT NULL,
  "devs_id" INTEGER NOT NULL,
  "id" INTEGER PRIMARY KEY NOT NULL,
  CONSTRAINT "fk_projects_has_devs_projects"
    FOREIGN KEY("projects_id")
    REFERENCES "projects"("id"),
  CONSTRAINT "fk_projects_has_devs_devs1"
    FOREIGN KEY("devs_id")
    REFERENCES "devs"("id")
);
CREATE INDEX "projects_has_devs.fk_projects_has_devs_devs1_idx" ON "projects_has_devs" ("devs_id");
CREATE INDEX "projects_has_devs.fk_projects_has_devs_projects_idx" ON "projects_has_devs" ("projects_id");
COMMIT;
