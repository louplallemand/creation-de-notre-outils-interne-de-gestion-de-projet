import { Request, Response } from "express-serve-static-core"
import HomeController from "./HomeController";

export default class addController
{
    static page(req: Request, res: Response): void
    {
        res.render('pages/add', {
            title: 'Gestion de projets',
        });
    }

    static add(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        let projectIdentifier = db.prepare('SELECT name FROM projects').all();
        projectIdentifier = projectIdentifier.map((e : any) => String(e.name));

        let projectName = req.body.projectName;
        let projectContent = req.body.projectContent;

        if (projectIdentifier.includes(projectName)){
            addController.addechec(req, res)
        }
        else{
        const createProject = db.prepare('INSERT INTO projects ("name", "content","clients_id") VALUES (?, ?, ?)').run(projectName, projectContent, 1)
        res.redirect('/')
        }
    }

    static addechec(req: Request, res: Response): void
    {
        res.render('pages/add', {
            title: 'Gestion de projets',
            alert: 'Ce nom de projet est déjà utilisé'
        });
    }
}