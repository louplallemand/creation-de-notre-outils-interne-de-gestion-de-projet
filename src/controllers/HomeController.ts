import { Request, Response } from "express-serve-static-core";

export default class HomeController
{
    static home(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const listProjects = db.prepare('SELECT * FROM projects').all();

        let colorcheat = 1
        listProjects.forEach((element : any ) => {
            colorcheat = -colorcheat
            if (colorcheat>0){
                element.color="white"
            }else{
                element.color="rgb(212, 212, 212)"
            }
        });
        res.render('pages/home', {
            title: 'Gestion de projets',
            list: listProjects,
        });
    }

    static delete(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM projects WHERE id = ?').run(req.params.id);

        res.redirect('/')
    }
}