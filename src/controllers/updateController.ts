import { Request, Response } from "express-serve-static-core"
import HomeController from "./HomeController";


export default class updateController
{
    static page(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        let titleProject = db.prepare('SELECT name FROM projects WHERE id=?').all(id)
        titleProject = titleProject.map((e : any) => String(e.name));
        let contentProject = db.prepare('SELECT content FROM projects WHERE id=?').all(id)
        contentProject = contentProject.map((e : any) => String(e.content));

        res.render('pages/updateproject', {
            title: 'Gestion de projets',
            titleProject: titleProject,
            contentProject: contentProject,
            id: id
        });
    } 

    static update (req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        let projectIdentifier = db.prepare('SELECT name FROM projects').all();
        projectIdentifier = projectIdentifier.map((e : any) => String(e.name));
        let projectBackup = db.prepare('SELECT content FROM projects').all();
        projectBackup = projectBackup.map((e :any) => String(e.content));

        let projectName = req.body.projectName;
        let projectContent = req.body.projectContent;

        if (projectName.length==0 || projectContent.length==0){
            updateController.updateechec(req, res)
        }
        else if(!projectBackup.includes(projectContent)){
            const updateProject = db.prepare('UPDATE projects SET name = ?, content = ? WHERE id = ?').run(projectName, projectContent, id);

            res.redirect('/')
        }
        else if(projectIdentifier.includes(projectName) && projectBackup.includes(projectContent)){
            updateController.updateused(req, res)
        }
        else{
            const updateProject = db.prepare('UPDATE projects SET name = ?, content = ? WHERE id = ?').run(projectName, projectContent, id);

            res.redirect('/')
        }

    }

    static updateechec(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        let titleProject = db.prepare('SELECT name FROM projects WHERE id=?').all(id)
        titleProject = titleProject.map((e : any) => String(e.name));
        let contentProject = db.prepare('SELECT content FROM projects WHERE id=?').all(id)
        contentProject = contentProject.map((e : any) => String(e.content));

        res.render('pages/updateproject', {
            title: 'Gestion de projets',
            titleProject: titleProject,
            contentProject: contentProject,
            id: id,
            alert: 'QUIT THE WIZARDRY DO NOT UPDATE TO NOTHING >:('
        });
    }

    static updateused(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        let titleProject = db.prepare('SELECT name FROM projects WHERE id=?').all(id)
        titleProject = titleProject.map((e : any) => String(e.name));
        let contentProject = db.prepare('SELECT content FROM projects WHERE id=?').all(id)
        contentProject = contentProject.map((e : any) => String(e.content));

        res.render('pages/updateproject', {
            title: 'Gestion de projets',
            titleProject: titleProject,
            contentProject: contentProject,
            id: id,
            alert: 'Ce nom est deja utilisé'
        });
    }
}