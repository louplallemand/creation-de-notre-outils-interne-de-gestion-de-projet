import { Request, Response } from "express-serve-static-core"


export default class projectController
{
    static page(req: Request, res: Response): void
    {
        const db = req.app.locals.db;
        const id = req.params.id;

        let titleProject = db.prepare('SELECT name FROM projects WHERE id=?').all(id)
        titleProject = titleProject.map((e : any) => String(e.name));
        let contentProject = db.prepare('SELECT content FROM projects WHERE id=?').all(id)
        contentProject = contentProject.map((e : any) => String(e.content));

        res.render('pages/project', {
            title: 'Gestion de projets',
            titleProject: titleProject,
            contentProject: contentProject
        });
    } 
}