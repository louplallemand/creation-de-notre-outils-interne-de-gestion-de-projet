import { Application } from "express";
import HomeController from "./controllers/HomeController";
import addController from "./controllers/addController";
import projectController from "./controllers/projectController";
import updateController from "./controllers/updateController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.home(req, res);
    });
    
    app.get('/add', (req, res)=>
    {
        addController.page(req, res);
    });
    app.post('/add', (req, res) =>
    {
        addController.add(req, res);
    });
    app.get('/add', (req, res)=>
    {
        addController.addechec(req, res);
    });
    app.get('/project/:id', (req, res)=>
    {
        req.params.id
        projectController.page(req, res);
    });
    app.get('/deleteproject/:id',(req, res)=>
    {
        HomeController.delete(req, res);
    });
    app.get('/updateproject/:id',(req, res)=>
    {
        req.params.id
        updateController.page(req, res);
    });
    app.post('/update/:id', (req, res) =>
    {
        req.params.id
        updateController.update(req, res);
    });
}
